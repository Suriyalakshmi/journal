from bs4 import BeautifulSoup
import json
Inputfile = open("journal.xml","r")
contents = Inputfile.read()
soup = BeautifulSoup(contents,'xml')

def surname(a):
  name={}
  if a.find('surname'):
    name['family']=a.find('surname').get_text()
  if a.find('given-names'):
    name['given']=a.find('given-names').get_text() 
  return name

def names(reference):
  author=[]
  if reference.find('person-group'):
    names=reference.find('person-group').find_all('name')
    for name in names:
      authorname=surname(name)
      author.append(authorname)
  return author

def collab(reference):
  ref_collab=[]
  if reference.find('collab'):
    col=reference.find('collab').get_text()
    ref_collab.append(col)
  return ref_collab

def titles(reference):
  title=[]
  if reference.find('article-title'):
    t=reference.find('article-title').text.replace(' ','').replace('\n',' ')
    title.append(t)
  return title

def volume(reference):
  vol=[]
  if reference.find('volume'):
    v=reference.find('volume').get_text()
    vol.append(v)
  return vol
 
def years(reference):
  year=[]
  if reference.find('year'):
    y=reference.find('year').get_text()
    year.append(y)
    return year

def type(reference):
  if reference['publication-type']:           
    types=reference['publication-type']
  return "article-"+types

ref=soup.find_all('element-citation')
references=[]
for ref_tag in ref:
  reference={}

  author=names(ref_tag)
  if author:
    reference['author']=names(ref_tag)

  ref_collab=collab(ref_tag)
  if ref_collab:
    reference['author']=collab(ref_tag)
  
  title=titles(ref_tag)
  if title:
    reference['title']=titles(ref_tag)
  
  vol=volume(ref_tag)
  if vol:
    reference['Volume']=volume(ref_tag)
  
  year=years(ref_tag)
  if year:
    reference['date']=years(ref_tag)
  
  reference['type']=type(ref_tag)
  references.append(reference)

final_out=json.dumps(references,indent=4)
print(final_out)

with open('Journal.json','w') as outfile :
    outfile.write(final_out)